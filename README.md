# JWT keystore helper

## Usage

```text
Usage: jwt-keystore-helper [-p]... [-ca=<certificateAlias>] [--iat=<iat>]
                           [--iss=<issuer>] [--rol=<roles>]
                           [--rol-claim-name=<rolesClaimName>]
                           [--sub=<subject>] <keystoreFile>
      <keystoreFile>    The keystore to load the certificate from
      -ca, --certificate-alias=<certificateAlias>
                        Alias of the certificate to load from the keystore
                        Used to extract the private key and sign the token with
                        If no alias is specified the first certificate found in
                          the keystore will be used
      --iat=<iat>       iat claim
      --iss=<issuer>    iss claim
  -p, --password        Password for the keystore
                        Default to an empty password
      --rol=<roles>     Comma delimited list of role names to be added as a
                          claim
      --rol-claim-name=<rolesClaimName>
                        Name of the claim for the roles
      --sub=<subject>   sub claim
```

### IntelliJ

Create a run configuration for the kotlin class `GenerateTokenCommand` executing the main function. The required command line arguments can be given in the "Program arguments" section inside the run configuration window.

![Example run config](doc/img/example_run_config.png)