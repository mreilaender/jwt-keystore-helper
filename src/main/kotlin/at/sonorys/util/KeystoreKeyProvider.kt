package at.sonorys.util

import com.auth0.jwt.interfaces.RSAKeyProvider
import java.io.File
import java.security.Key
import java.security.KeyStore
import java.security.PublicKey
import java.security.cert.Certificate
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.*

class KeystoreKeyProvider(
    keystoreFile: File,
    private val password: CharArray,
    private val certificateAlias: Optional<String>
) :
    RSAKeyProvider {

    private lateinit var keyStore: KeyStore

    init {
        loadKeystore(keystoreFile)
    }

    private fun loadKeystore(keystoreFile: File) {
        keyStore = KeyStore.getInstance(keystoreFile, password)
    }

    override fun getPublicKeyById(keyId: String?): RSAPublicKey {
        val certificateAlias: String = getCertificateAliasToUse(keyId)

        val certificate: Certificate = keyStore.getCertificate(certificateAlias)
            ?: throw IllegalStateException("No certificate found for alias $certificateAlias")

        val publicKey: PublicKey = certificate.publicKey

        verifyIsRSAKeyOrThrow(publicKey)

        return publicKey as RSAPublicKey
    }

    private fun getCertificateAliasToUse(keyId: String?): String {
        if(keyId != null) {
            return keyId
        } else if (this.certificateAlias.isPresent) {
            return this.certificateAlias.get()
        }

        return getFirstCertificateAlias()
    }

    private fun getFirstCertificateAlias(): String {
        return keyStore.aliases().nextElement()
    }

    override fun getPrivateKey(): RSAPrivateKey {
        val certificateAlias: String = getCertificateAliasToUse(null)

        val key: Key = keyStore.getKey(certificateAlias, password)
            ?: throw IllegalStateException("No certificate found for alias $certificateAlias")

        verifyIsRSAKeyOrThrow(key)

        return key as RSAPrivateKey
    }

    private fun verifyIsRSAKeyOrThrow(key: Key) {
        val requiredAlgorithmName = "RSA"
        val foundAlgorithm = key.algorithm
        if (foundAlgorithm != requiredAlgorithmName) {
            throw IllegalStateException("Invalid algorithm for public key, found [$foundAlgorithm] but required [$requiredAlgorithmName]")
        }
    }

    override fun getPrivateKeyId(): String {
        return getCertificateAliasToUse(null)
    }

    fun clearPassword() {
        clearArray(password)
    }
}