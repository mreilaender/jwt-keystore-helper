package at.sonorys.util

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import picocli.CommandLine
import java.io.File
import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.Callable
import kotlin.system.exitProcess

fun main(args: Array<String>): Unit =
    exitProcess(CommandLine(GenerateTokenCommand()).execute(*args))

fun clearArray(charArray: CharArray) {
    Arrays.fill(charArray, ' ')
}

@CommandLine.Command(name = "jwt-keystore-helper")
class GenerateTokenCommand : Callable<Int> {

    @CommandLine.Parameters(
        index = "0", description = ["The keystore to load the certificate from"]
    )
    lateinit var keystoreFile: File

    @CommandLine.Option(
        names = ["--password", "-p"],
        interactive = true,
        description = ["Password for the keystore", "Default to an empty password"]
    )
    var keystorePassword: CharArray = charArrayOf()

    @CommandLine.Option(
        names = ["--certificate-alias", "-ca"],
        description = ["Alias of the certificate to load from the keystore",
            "Used to extract the private key and sign the token with",
            "If no alias is specified the first certificate found in the keystore will be used"]
    )
    var certificateAlias: Optional<String> = Optional.empty()

    @CommandLine.Option(names = ["--iss"], description = ["iss claim"])
    var issuer: Optional<String> = Optional.empty()

    @CommandLine.Option(
        names = ["--rol"],
        description = ["Comma delimited list of role names to be added as a claim"]
    )
    var roles: Optional<String> = Optional.empty()

    @CommandLine.Option(
        names = ["--rol-claim-name"], description = ["Name of the claim for the roles"],
        defaultValue = "rol"
    )
    var rolesClaimName: String = "rol"

    @CommandLine.Option(names = ["--sub"], description = ["sub claim"])
    var subject: Optional<String> = Optional.empty()

    @CommandLine.Option(names = ["--iat"], description = ["iat claim"])
    var iat: Optional<Long> = Optional.empty()

    override fun call(): Int {
        val keyProvider = KeystoreKeyProvider(keystoreFile, keystorePassword, certificateAlias)

        val builder = JWT.create()

        roles.ifPresent {
            builder.withArrayClaim(rolesClaimName, it.split(",").toTypedArray())
        }

        issuer.ifPresent { builder.withIssuer(it) }
        subject.ifPresent { builder.withSubject(it) }
        iat.ifPresentOrElse({ builder.withIssuedAt(Date(it)) },
            { builder.withIssuedAt(Timestamp.valueOf(LocalDateTime.now())) })
        iat.ifPresent { builder.withIssuedAt(Date(it)) }

        println(builder.sign(Algorithm.RSA256(keyProvider)))

        keyProvider.clearPassword()
        clearKeystorePassword()

        return 0
    }

    private fun clearKeystorePassword() {
        clearArray(keystorePassword)
    }
}